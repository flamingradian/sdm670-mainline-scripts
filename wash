#!/bin/sh
# SPDX-License-Identifier: MIT

channel="$(basename "$(pwd)")"
commits="$(git show -s --reverse --pretty=format:%H tail-"$channel"..$1)"

user_resolve_cpick() {
	while [ -f ../linux.git/worktrees/fixup/CHERRY_PICK_HEAD ]
	do
		echo "Please resolve the conflicts and run git cherry-pick --continue."
		if ! bash -i
		then
			git cherry-pick --abort
			exit 1
		fi
	done
}

user_resolve_rebase() {
	while [ -f ../linux.git/worktrees/fixup/REBASE_HEAD ]
	do
		echo "Please resolve the conflicts and run git rebase --continue."
		if ! bash -i
		then
			git rebase --abort
			exit 1
		fi
	done
}

user_resolve_diff() {
	while ! git diff --exit-code "$commit" > /dev/null
	do
		echo "Please resolve the differences between this and $commit."
		if ! bash -i
		then
			exit 1
		fi
	done
}

amend_clean_commit() {
	target="$(git show -s --pretty=format:%H clean-"$channel"^{/^"$(git show -s --pretty=format:%b $commit | grep -Po '(?<=^Amends: ).+' | sed -e 's/[!$()*+.?[\^{|}]/\\&/g')"})"

	pushd ../fixup

	git checkout -f "$target"

	echo "Apply changes from $commit"
	git cherry-pick "$commit"
	if ! user_resolve_cpick
	then
		popd
		return 1
	fi

	git reset --soft HEAD~1
	if ! git diff --exit-code HEAD~1 > /dev/null
	then
		git commit --amend
	else
		git reset --hard HEAD~1
	fi

	echo "Rebase patches from $old_head to $target"
	if [ -e /home/r2/sdm670-mainline/linux.git/worktrees/fixup/rebase-merge ]
	then
		git rebase --quit
	fi
	git rebase --onto HEAD "$target" "$old_head"
	if ! user_resolve_rebase
	then
		popd
		return 1
	fi

	echo "Check that the commits are the same"
	if ! user_resolve_diff
	then
		popd
		return 1
	fi

	git branch -f clean-"$channel"

	popd
}

insert_commit() {
	target="$(git show -s --pretty=format:%H clean-"$channel"^{/^"$(git show -s --pretty=format:%b $commit | grep -Po '(?<=^Parent: ).+' | sed -e 's/[!$()*+.?[\^{|}]/\\&/g')"} || $old_head)"

	pushd ../fixup

	git checkout -f "$target"

	echo "Apply changes from $commit"
	git cherry-pick "$commit"
	if ! user_resolve_cpick
	then
		popd
		return 1
	fi

	if git show -s | grep 'Parent: '
	then
		git show -s --pretty=format:'%s

%b' | grep -v "^Parent: " | git commit --amend -F -
	fi

	echo "Rebase patches from $old_head to $target"
	git rebase --onto HEAD "$target" "$old_head"
	if ! user_resolve_rebase
	then
		popd
		return 1
	fi

	echo "Check that the commits are the same"
	if ! user_resolve_diff
	then
		popd
		return 1
	fi

	git branch -f clean-"$channel"

	popd
}

if [ "$(head -c 16 ../patches/export/.git/HEAD)" = "ref: refs/heads/" ]
then
	orig_branch="$(tail -c+17 ../patches/export/.git/HEAD)"
fi

git -C ../patches/export checkout tail

for commit in $commits
do
	old_head="$(git show --no-patch --pretty=format:%H clean-"$channel")"

	if git show -s --pretty=format:%b $commit | grep -Po '(?<=^Amends: ).+'
	then
		amend_clean_commit || break
	else
		insert_commit || break
	fi

	../quash "$channel"
	git -C ../patches/export add list-"$channel" "$channel"
	git -C ../patches/export commit -e \
		-m "$(git show -s --pretty=format:'%s

%b' "$commit")" \
		--author="$(git show -s --pretty=format:"%an <%ae>" "$commit")" \
		--date="$(git show -s --pretty=format:%ai "$commit")"

	git branch -f tail-"$channel" $commit
done

if [ -n "$orig_branch" ]
then
	git -C ../patches/export checkout -B "$orig_branch"
fi
